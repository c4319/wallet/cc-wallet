## State of affairs
The cc-wallet is currently a prototype that manages addresses for all CommunityCoin networks and invited networks (except KMD at this time)See https://gitlab.com/c4319/cc-index for envisioned networks). It can generate raw transactions for sending CommunityCoins, (except KMD and RUBTC at this time). The wallet is connected to a price feed and includes a calculator, enabling it to operate in most markets. Despite intensive experimentation with the ElectrumX middle layer, there is no satisfactory solution for connecting directly to individual cc-networks.

## Some history of ElectrumX
ElectrumX allows users to run their own Electrum server, which connects to a full node and indexes the blockchain, facilitating efficient querying of the history of arbitrary addresses. The project was developed by Neil Booth, who built upon the work of Thomas Voegtlin, creator of the Electrum software, with code also based on Abe and Pywallet. In October 2020, Neil Booth dropped support for Bitcoin, citing the project's increasingly conservative and rigid approach. He shifted focus to BCH and other forks, anticipating a future with larger block sizes. For continued support of Bitcoin, see the ElectrumX forks: https://github.com/kyuupichan/electrumx and https://github.com/spesmilo/electrumx.

## The Problem:
ElectrumX is a Moderately large (0,5) but technical software package based on python that requires substantial maintenance. Its interface is complex, while a light client only requires two or three functions. For CommunityCoins, only a few Electrum servers exist, and they are generally unstable. Moreover, these servers typically lack an active WebSocket interface, making them inaccessible to browser-based applications (PWA). ElectrumX also lacks proof of trust, relying on a peer discovery system without validity checking. Light clients need additional features that ElectrumX does not provide, such as access to market prices.

## Proposed Solution: RingOfTrust
ElectrumX operates on a function-based model, while a light client only needs access to the following data:

- access through port 80
- The balance of a legacy address.
- The unspent transactions associated with it.
- The ability to submit raw transactions to the network.
- Market prices, which are a collection of Tickers-AssetNames-Prices expressed in Bitcoin based on an average market view.
- The indexing task can be efficiently handled with about 100 lines of code, as demonstrated in this GitHub repository. This means that only legacy addresses will be indexed, so the cc-wallet will only support legacy addresses as inputs. Currently, we have two price feeders (ecoincore and marketmerge) and a price determination proposal that could be integrated within the RingOfTrust.

To verify the integrity of a new indexing service, each new legacy output can be hashed using the input transaction ID, the output index, and the previous hash, starting with the genesis hash. To accommodate orphans and forks, this hash should lag behind the chain tip, maintaining several tips in a row to allow for slight synchronization differences. Newer outputs will not be consolidated but kept in a mempool.